import React from 'react';
import MainPage from './Components/MainPage/MainPage';
import { Switch, Route } from 'react-router-dom';
import OtherComponent from './Components/MainPage/OtherComponent/OtherComponent';

function App() {

  return (

    <Switch>
      <Route path="/other" component={ OtherComponent }/>
      <Route path="/" component={ MainPage } />
    </Switch>

  );
}

export default App;

