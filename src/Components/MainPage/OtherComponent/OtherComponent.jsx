import React from 'react';

const OtherComponent = () => {

    return (
        <div>
            <h2>
                It Is Other Component!
            </h2>
        </div>
    );
};

export default OtherComponent;