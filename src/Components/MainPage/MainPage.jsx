import React from 'react';
import MiddleArea from './MiddleArea/MiddleArea';
import { Link } from 'react-router-dom';

const MainPage = () => {
    return (
        <React.Fragment>
            <h1 style={{ textAlign: 'center' }}>It Is Main Page!</h1>

            <Link to="/other">Other Component!</Link> 

            <MiddleArea style={{display: 'flex'}} />
        </React.Fragment>
    );
};

export default MainPage;
