import React from 'react';
import LeftBar from './LeftBar/LeftBar';
import RightBar from './RightBar/RightBar';

const MiddleArea = () => {
    return (
        <div style={{display: 'flex'}}>
            <LeftBar />
            <RightBar/>
        </div>
    );
};

export default MiddleArea;