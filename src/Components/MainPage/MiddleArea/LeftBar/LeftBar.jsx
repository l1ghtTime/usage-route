import React from 'react';
import { Link } from 'react-router-dom';


const LeftBar = () => {
    return (
        <nav style={{border: '2px solid #ccc', width: '50%'}} >
            <ul>
                <li>
                    <Link to="/">Home</Link>
                </li>

                <li>
                    <Link to="/story">Story</Link>
                </li>

                <li>
                    <Link to="/posts">Posts</Link>
                </li>
            </ul>
        </nav>
    );
};

export default LeftBar;