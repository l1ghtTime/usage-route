import React from 'react';
import { Switch, Route } from 'react-router-dom';
import HomeContent from './HomeContent/HomeContent';
import StoryContent from './StoryContent/StoryContent';
import PostsContent from './PostsContent/PostsContent';

const RightBar = () => {
    return (    
        <Switch>
            <Route exact path="/" component={ HomeContent } />
            <Route path="/story" component={ StoryContent } />
            <Route path="/posts" component={ PostsContent } />
        </Switch>
    );
};

export default RightBar;